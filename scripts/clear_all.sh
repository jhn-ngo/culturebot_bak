#!/bin/bash -v
rm -rf /var/culturebot/*

curl -XDELETE localhost:9200/*

( cd /opt/grakn-core-1.4.2/; ./graql console -k culturebot << EOL
clean
confirm
EOL
)

( cd /opt/grakn-core-1.4.2/; ./graql console -f /opt/culturebot/src/graph/schema.gql  -k culturebot)

