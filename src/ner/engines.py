import requests
import os
import spotlight
import json
import logging
from sner import Ner

from config import Config
from utils.profiler import Profile
from utils.cache import NERCache

class NEREngine:
    def __init__(self, type, cache=True):
        self.cache = NERCache() if cache else None
        self.type = type

    def get_cache(self, query):
        if not self.cache: return None
        logging.info("Getting cache, type: {}, query: {}".format(self.type, query))
        return self.cache.get(self.type, query)

    def set_cache(self, query, results):
        logging.info("Setting cache, type: {}, query: {}, results: {}".format(self.type, query, results))
        return self.cache.set(self.type, query, results)


class NERGoogleGraph(NEREngine):
    URL = "https://kgsearch.googleapis.com/v1/entities:search"

    def __init__(self, cache=True):
        super().__init__("google", cache)

    def extract(self, text, language='en'):
        Profile.getProfile("Google::extract").start()
        cached = self.get_cache(text)
        if cached:
            res = cached
        else:
            options = {
                "limit": 1,
                "key": Config.GOOGLE_KG_KEY,
                "query": text,
                "languages": language
            }
            res = requests.get(self.URL, params=options)
            res = json.loads(res.content.decode('utf-8'))
            self.set_cache(text, res)
        item = res.get('itemListElement')
        Profile.getProfile("Google::extract").stop()
        if not item: return []
        return item[0]['result']
        #return jsonld.expand(res.content.decode('utf-8'))

class NERDbPedia(NEREngine):
#    URL = "http://model.dbpedia-spotlight.org/{}/annotate"
#    URL = "http://localhost:2222/rest/annotate"

    def __init__(self, url="http://model.dbpedia-spotlight.org/{}/annotate", confidence=0.5, cache=True):
        super().__init__("dbpedia", cache)
        self.url = url
        self.confidence = confidence

    def extract(self, text, language='en'):
        Profile.getProfile("DBPedia::extract").start()
        options = {
            "confidence": self.confidence,
#            "filters": {
#                "types": "Person,Organization,Place"
#            }
        }
        Profile.getProfile("DBPedia::extract::annotate").start()
        try:
            #cached = self.get_cache(text)
            #if cached:
            #    res = cached
            #else:
                url = self.url.format(language)
                print("Annotating URL {}".format(url))
                res = spotlight.annotate(url, text, **options)
            #    self.set_cache(text, res)
        except spotlight.SpotlightException:
            res = []
        Profile.getProfile("DBPedia::extract::annotate").stop()
        print("RES {}".format(res))
        details = []
        Profile.getProfile("DBPedia::extract::get_details").start()
        for r in res:
           details.append({"entity" : r, "details" : self.get_details(r)})
        Profile.getProfile("DBPedia::extract::get_details").stop()
        Profile.getProfile("DBPedia::extract").stop()
        return details

    def get_details(self, result):
        cached = self.get_cache_details(result['URI'])
        if cached: return cached

        from SPARQLWrapper import SPARQLWrapper, JSON
        query = """
                      SELECT ?primaryTopic ?thumbnail ?type WHERE
                      {
                          # give me ?type of the resource
                          <"""+result['URI']+ """> foaf:isPrimaryTopicOf ?primaryTopic .
                          <"""+result['URI']+ """>  dbo:thumbnail ?thumbnail .
                          <"""+result['URI']+ """>  rdf:type ?type
                          # interested only in DBPedia ontology types 
                         FILTER ( strstarts(str(?type), "http://dbpedia.org/ontology/") )

                     }
        """
        print("QUERY: {}".format(query))
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        sparql.setReturnFormat(JSON)

        sparql.setQuery(query)  # the previous query as a literal string

        qres = sparql.query().convert()['results']['bindings']
        # Weird situation but happens - no wikipedia page is returned, try to guess it
        if not qres:
            # TODO: language-dependent
            qres = [{'primaryTopic' : {'value' : result['URI'].replace('http://dbpedia.org/resource/', 'https://en.wikipedia.org/wiki/')}}]
        self.set_cache_details(result['URI'], qres)
        return qres

    def get_cache_details(self, query):
        if not self.cache: return None

        res =  self.cache.get("dbpedia_details", query)
        logging.info("Getting cache, type: {}, query: {}, results: {}".format("dbpedia_details", query, res))
        return res

    def set_cache_details(self, query, results):
        return self.cache.set("dbpedia_details", query, results)


class NERStanford(NEREngine):

    STANFORD_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'tools',
                                 'stanford-ner-2017-06-09')
    def __init__(self, caseless=False, cache=True):
        super().__init__("stanford", cache)
        os.environ['CLASSPATH'] = self.STANFORD_PATH
        print(self.STANFORD_PATH)
        #model = 'english.all.3class.caseless.distsim.crf.ser.gz' if caseless else 'english.all.3class.distsim.crf.ser.gz'
        model = "english.conll.4class.distsim.crf.ser.gz"
        #self.ner = StanfordNERTagger(self.STANFORD_PATH + '/classifiers/{}'.format(model))
        self.ner = Ner(host='localhost', port=9199)

    def extract(self, text, language='en'):
        Profile.getProfile("Stanford::extract").start()
        #tokens = nltk.word_tokenize(text)
        Profile.getProfile("Stanford::extract::tag").start()
        cached = self.get_cache(text)
        if cached:
            iob = cached
        else:
            iob = self.ner.get_entities(text)#self.ner.tag(tokens)
            self.set_cache(text, iob)
        Profile.getProfile("Stanford::extract::tag").stop()
        entities = []
        #print("IOB: {}".format(iob))
        entity = ""
        etag = ""
        for token,tag in iob:
#          print("Token: {}, Tag: {}".format(token,tag))
          if token == ',':
             continue
          elif (tag == 'O' or  etag != tag) and entity: 
             entities.append((entity.strip(),etag))
             entity = ""
             etag = ""
          elif tag != 'O':
             entity += ' ' + token
             etag = tag
          else:
             entity = ""
             etag = ""
#          print("ENT: {}, ETAG: {}".format(entity, etag))
        # Handle last record
        if entity: 
           entities.append((entity.strip(),etag))
        Profile.getProfile("Stanford::extract").stop()
        return entities
                 


class NER:
    engines = {'google': NERGoogleGraph(),
               'dbpedia': NERDbPedia(),
               'stanford': NERStanford()}

    def extract(self, text, language='en'):
        stanford_entities = self.engines['stanford'].extract(text, language)
        print("NER -> {}".format(stanford_entities))
        google_entities = []
        for entity,tag in stanford_entities:
            if tag == 'PERSON' and len(entity.split()) < 2:
                print("Skipping 1-word person name: {}".format(entity))
                continue
            #print("Querying google for {}".format(entity))
            google_entities.append(self.engines['google'].extract(entity, language))
        #print("GNER -> ");
        #pprint.pprint(google_entities)
        dbpedia_entities = self.engines['dbpedia'].extract(text, language)
        #print("DBNER -> ");
        #pprint.pprint(dbpedia_entities)
        return {'google': google_entities,
                'dbpedia': dbpedia_entities}
#        return {'dbpedia': dbpedia_entities}

if __name__ == "__main__":
    import sys
    from pprint import pprint
    #ng = NERGoogleGraph()
    #pprint(ng.extract(sys.argv[1]))
    nb = NERDbPedia()
    pprint(nb.extract(sys.argv[1], 'en'))

