## debug
* debug
  - action_toggle_debug

## unguided
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.no
  - utter_greetings.bye

## unguided (2)
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.yes
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results


## unguided_related
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* related{"relating-object-id" : "europeana:/90402/SK_C_597"}
   - action_search_related
   - slot{"related-search-results" : "search results"}
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results

## unguided_related (2)
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* related{"relating-object-id" : "europeana:/90402/SK_C_597"}
   - action_search_related
   - slot{"related-search-results" : "search results"}
* related{"relating-object-id" : "europeana:/90402/SK_C_597"}
   - action_search_related
   - slot{"related-search-results" : "search results"}


## unguided_no
* search{"search-fields": "some value"}
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.no
  - utter_greetings.bye


## get started
* get_started
  - utter_onboarding
  - utter_what_are_you_looking_for

## hello
* greetings.hello
  - utter_onboarding

## stats
* stats
  - action_stats

## guided search
* guided_search
  - action_init_guided_search
> guided_search

## guided_search (fallback)
* default
  - action_not_understood
  - slot{"guided-search" : true}
  - action_init_guided_search

## guided search (book)
> guided_search
* inform{"gs_object_type" : "book"}
  - form_object_type
  - form_book
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
  
## guided search (photograph)
> guided_search
* inform{"gs_object_type" : "photograph"}
  - form_object_type
  - form_photograph
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results

## guided search (painting)
> guided_search
* inform{"gs_object_type" : "painting"}
  - form_object_type
  - form_painting
  - followup{"name" :"action_search"}
  - slot{"search-results": "found"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* show_more
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.no
  - utter_greetings.bye

## guided search / no
> guided_search
* inform{"gs_object_type" : "painting"}
  - form_object_type
  - form_painting
  - followup{"name" :"action_search"}
  - slot{"search-results": "found"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.no
  - utter_greetings.bye

## guided search / yes
> guided_search
* inform{"gs_object_type" : "painting"}
  - form_object_type
  - form_painting
  - followup{"name" :"action_search"}
  - slot{"search-results": "found"}
  - followup{"name" :"action_search_results"}
  - action_search_results
* confirmation.yes
  - action_search
  - slot{"search-results": "some value"}
  - followup{"name" :"action_search_results"}
  - action_search_results


## guided search / no results
> guided_search
* inform{"gs_object_type" : "painting"}
  - form_object_type
  - form_painting
  - followup{"name" :"action_search"}
  - slot{"search-results": null}
  - followup{"name" :"action_no_results"}
  - action_no_results
* confirmation.yes
  - action_init_guided_search

## guided search / no results (2)
> guided_search
* inform{"gs_object_type" : "painting"}
  - form_object_type
  - form_painting
  - followup{"name" :"action_search"}
  - slot{"search-results": null}
  - followup{"name" :"action_no_results"}
  - action_no_results
* confirmation.no
  - utter_greetings.bye



## Generated Story 486845982445278085
* greetings.hello
    - utter_onboarding
* guided_search
    - action_init_guided_search
* inform{"gs_object_type": "book"}
    - slot{"gs_object_type": "book"}
    - form_book
    - action_search
    - slot{"search-results": "some value"}
    - followup{"name" :"action_search_results"}
    - action_search_results

## search related objects
* related{"relating-object-id" : "europeana:/90402/SK_C_597"}
    - action_search_related
    - slot{"related-search-results" : "search results"}