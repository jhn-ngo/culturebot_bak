import os

from rasa_core.tracker_store import MongoTrackerStore
from rasa_core.agent import Agent
from rasa_core.utils import EndpointConfig
from rasa_core.domain import TemplateDomain

from chatbot.nlu.interpreter import CultureBotInterpreter
from chatbot.tests.nlg import SwitchTemplatedNaturalLanguageGenerator


class Bot:
    ACTIONS_PORT=5077

    def __init__(self, model_path="../models/"):
        domain = TemplateDomain.load(os.path.join(model_path, "dialogue", "domain.yml"))
        # "models/nlu/current"
        #self.actions_server = subprocess.Popen("python3 -m rasa_core_sdk.endpoint --actions actions -p {}".format(self.ACTIONS_PORT).split())
        action_endpoint = EndpointConfig("http://localhost:{}/webhook".format(self.ACTIONS_PORT)) # TODO : read from endpoints.yml

        self.rasa_agent = Agent.load(os.path.join(model_path, "dialogue"),
                           interpreter=CultureBotInterpreter(os.path.join(model_path, "nlu", "current")),
                           action_endpoint=action_endpoint,
                           tracker_store=MongoTrackerStore(None),
                           generator=SwitchTemplatedNaturalLanguageGenerator(domain.templates,
                                                                                       templates_off=True),
                                     )

    def reset(self):
        self.say("/restart")

    def say(self, text):
        texts =  [m["text"] for m in self.rasa_agent.handle_text(text)]
        out_texts = []
        for t in texts:
            if t.startswith("utter_"):
                out_texts.append(t)
            else:
                tt = t.split(":")
                if len(tt) >= 2:
                    out_texts.append("title : subtitle")
                else:
                    out_texts.append(t)

        return out_texts


