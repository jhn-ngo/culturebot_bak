from rasa_core.nlg.template import TemplatedNaturalLanguageGenerator
import re
# Enable turning on/off template generations
class SwitchTemplatedNaturalLanguageGenerator(TemplatedNaturalLanguageGenerator):

    def __init__(self, templates, templates_off=False):
        super().__init__(templates)
        self.templates_off = templates_off

    def generate(self, template_name, tracker, output_channel, **kwargs):
        # If templates are switched off, return template name
        if self.templates_off:
            return {"text": self._generate_template(template_name, tracker, **kwargs)}
        return super().generate(template_name, tracker, output_channel, **kwargs)


    def _generate_template(self, template_name, tracker, **kwargs):
        template = self.templates[template_name][0] # pick the first utterance (all should be equivalent)
        vars = re.findall(r"\{([A-za-z0-9_]+)\}", template["text"])
        template_vars = self._template_variables(tracker.current_slot_values(), kwargs)

        vars_text = ",".join(['{}="{}"'.format(k,v) for k,v in template_vars.items() if k in vars])
        if vars_text:
            return template_name + "{" + vars_text + "}"
        return template_name


