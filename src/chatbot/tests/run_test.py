import unittest
import requests
import json
import logging
from collections import namedtuple
import pytz
import re
import xmlrunner

from chatbot.tests.bot import Bot

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)

TEST_COUNTER = 1
TESTS_ALL = 0
class ConversationTestBase(unittest.TestCase):
    longMessage = True

    bot = Bot()

class Conversation:
    Utterance = namedtuple('Utterance', ['type', 'text'])
    def __init__(self, title):
        self.utterances = []
        self.title = title
        self.setup_actions = []
        self.teardown_actions = []

    def setup(self, bot):
        for action in self.setup_actions:
            globals()[action["name"]](self, bot, **action["params"])

    def teardown(self, bot):
        for action in self.teardown_actions:
            globals()[action["name"]](self, bot, **action["params"])

    def bot_uttered(self, utterances):
        self._uttered('bot', utterances)

    def user_uttered(self, utterances):
        self._uttered('user', utterances)

    def _uttered(self, type, utterances):
        if not isinstance(utterances, (list,tuple)):
            utterances = [utterances]
        for u in utterances:
            logging.warning("{} uttered: {}".format(type, u))
            self.utterances.append(Conversation.Utterance(type, u))

    def to_str(self):
        test_list = []
        test_list.append("## {}".format(self.title))
        for u in self.utterances:
            if u.type == "user":
                test_list.append("* {}".format(u.text))
            else:
                test_list.append("  - {}".format(u.text))
        return "\n".join(test_list)

def make_test_conversation_function(conv: Conversation):
    def setup(self):
        logging.warning("========== BEGIN TEST {}/{} =============".format(TEST_COUNTER, TESTS_ALL))
        self.bot.reset()

        logging.warning("===== TEST: {} =====".format(conv.title))
        logging.warning("-- >Setting up")
        conv.setup(self.bot)

    def teardown(self):
        logging.warning("-- >Tearing down")
        conv.teardown(self.bot)
        logging.warning("========== END TEST {} =============".format(TEST_COUNTER))
        globals()["TEST_COUNTER"] += 1
        logging.warning("===== END TEST: {} =====".format(conv.title))

    def test(self):
        answers = []
        for u in conv.utterances:
            if u.type == "user":
                self.assertEqual(answers, [], "Extra bot utterance(s)") # make sure all previous bot utterances were the same
                answers = self.bot.say(u.text)
                logging.debug("Said to bot: {}, received answer: {}".format(u.text, answers))
            else:
                self.assertNotEqual(answers, [], "Missing bot utterance(s)")
                self.assertEqual(answers[0].strip(), u.text.strip(), "Different utterances")
                answers = answers[1:] # shift next
    return setup,test,teardown



class ConversationTestLoader:
    START_COMMENT = "<!--"
    END_COMMENT = "-->"

    @staticmethod
    def load(test_file):
        conversations = []
        conv = None
        is_commented = False
        with open(test_file, "r") as tf:
            for line in tf:
                line = line.strip()
                # Handle comments (simple comments
                line, is_commented = ConversationTestLoader._handle_comments(line, is_commented)
                if not line: continue

                if line.startswith("##"):
                    if conv:
                        conversations.append(conv)
                    title = line[2:].strip()
                    conv = Conversation(title)
                    logging.warning("Loading conversation test: {}".format(title))
                elif line.startswith(">"):
                    action = ConversationTestLoader._parse_action(line)
                    logging.warning("Loading action: {}, action: {}".format(line, action))
                    if len(conv.utterances) == 0: # setup
                        conv.setup_actions.append(action)
                    else:
                        conv.teardown_actions.append(action)
                elif line.startswith("*"):
                    conv.user_uttered(line[1:].strip())
                elif line.startswith("-"):
                    conv.bot_uttered(line[1:].strip())
        if conv:
            conversations.append(conv)
        globals()["TESTS_ALL"]  = len(conversations)
        return conversations

    @staticmethod
    def _parse_action(line):
        m = re.search("([0-9A-Za-z_]+)\s*({(.*)})?", line)
        if not m: raise Exception("Invalid action line format: {}".format(line))
        action_dict = {"name": m.group(1),
                       "params": dict()}
        params = m.group(3)
        if not params: return action_dict
        for param in params.split(","):
            pname,pvalue = param.split("=")
            action_dict["params"][pname.strip()] = pvalue.strip(' "')
        return action_dict

    @staticmethod
    def _handle_comments(line, is_commented):
        start = line.find(ConversationTestLoader.START_COMMENT)
        if start >= 0:
            line = line[:start]
            return line, True
        end = line.find(ConversationTestLoader.END_COMMENT)
        if end >= 0:
            line = line[end+len(ConversationTestLoader.END_COMMENT):]
            return line, False
        if is_commented:
            return "", True
        return line,False


def reset(conv: Conversation, bot: Bot, time: str):
    bot.say("/reset")

if __name__ == '__main__':
    import sys, os
    import datetime

    test_file = sys.argv.pop()
    convs = ConversationTestLoader.load(test_file)
    suite = unittest.TestSuite()
    for conv in convs:
        setup_func,test_func,teardown_func = make_test_conversation_function(conv)
        classname = 'Test_{0}'.format(conv.title)
        if len(sys.argv) > 1 and classname not in sys.argv: continue
        runMethod = 'test_gen_{0}'.format(conv.title)
        globals()[classname] = type(classname,
                                   (ConversationTestBase,),
                                   {runMethod : test_func,
                                    'setUp': setup_func,
                                    'tearDown': teardown_func})
        suite.addTest(globals()[classname](runMethod))

    #unittest.main(testRunner=HTMLTestRunner(output='test_out'))
    #runner = HTMLTestRunner(output='test_out')
    #runner.run(suite)
    out_xml = 'reports/rasa_test_{}_{}.xml'.format(test_file, datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S"))
    with open(out_xml, 'wb') as output:
        testRunner = xmlrunner.XMLTestRunner(output=output)
        testRunner.run(suite)
        #unittest.main(
        #    testRunner=xmlrunner.XMLTestRunner(output=output),
        #    failfast=False, buffer=False, catchbreak=False)
    # os.system("junit-viewer --results={} | bcat".format(out_xml))