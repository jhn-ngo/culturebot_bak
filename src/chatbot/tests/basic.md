## guided_search
* hello
  - utter_onboarding{name="visitor"}
* /guided_search 
  - utter_ask_gs_object_type    
* /inform{"gs_object_type": "painting"}
  - utter_ask_gs_author{gs_author_type="painter"}
* Vermeer
  - utter_ask_gs_title
* *
  - utter_search{query="paintings by Vermeer"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more


## unguided_search
* I am looking for paintings by Vermeer
  - utter_search{query="paintings by Vermeer"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
  
## related  
* I am looking for paintings by Rembrandt
  - utter_search{query="paintings by Rembrandt"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
* /related{"relating-object-id": "heritageObject:europeana:/90402/SK_C_597"}
  - utter_search_related
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle


## show_more  
* I am looking for paintings by Rembrandt
  - utter_search{query="paintings by Rembrandt"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
* /show_more
  - utter_search{query="paintings by Rembrandt"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more


## search_lowercase
* I am looking for paintings by rembrandt
  - utter_search{query="paintings by rembrandt"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more

## fallback_search
* I am looking for madrid theme
  - utter_search{query="objects with  madrid theme"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more

## fallback_search_2
* rotterdam
  - utter_search{query="objects of rotterdam"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more

## place_search
* show me amsterdam
  - utter_search{query="objects of amsterdam"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
* no
  - utter_greetings.bye

## place_search_yes
* show me amsterdam
  - utter_search{query="objects of amsterdam"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
* yes
  - utter_search{query="objects of amsterdam"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more

  
## place_search_related
* show me amsterdam
  - utter_search{query="objects of amsterdam"}
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - utter_ask_show_more
* /related{"relating-object-id": "heritageObject:europeana:/90402/SK_C_597"}
  - utter_search_related
  - utter_show_results{objects="objects"}
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  - title : subtitle
  

## place_search_dedup
* I am looking for Madrid paintings
  - utter_search{query="paintings of Madrid"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - utter_ask_show_more

## place_search_no
* I am looking for Madrid paintings
  - utter_search{query="paintings of Madrid"}
  - utter_show_results{objects="paintings"}
  - title : subtitle
  - utter_ask_show_more
* no
  - utter_greetings.bye