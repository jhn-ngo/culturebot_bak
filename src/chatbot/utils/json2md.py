from rasa_nlu.training_data import load_data
import argparse

def parse_arguments():
    """Parse all the command line arguments for the run script."""
    parser = argparse.ArgumentParser(
        description='Convert Rasa NLU Training data from JSON to MD ')
    parser.add_argument(
        '-i', '--input',
        required=True,
        help="Input JSON")
    parser.add_argument(
        '-o', '--output',
        required=True,
        help="Output MD")


    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    with open(args.output, 'w') as f:
        f.write(load_data(args.input).as_markdown())
