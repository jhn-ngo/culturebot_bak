from rasa_core_sdk import Action
from rasa_core_sdk.forms import FormAction
from rasa_core_sdk.events import SlotSet, FollowupAction
from rasa_core.dispatcher import Dispatcher
from rasa_core.trackers import DialogueStateTracker
from rasa_core.domain import Domain
import logging
import json
from collections import defaultdict
from config import Config
from graph.kngraph import Graph

class Translator:
    @staticmethod
    def translate(text, lang="en"):
        if lang == "en": return text
        # TODO: translate
        return text

class ActionToggleDebug(Action):
    def name(self):
        return "action_toggle_debug"

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        debug = not tracker.get_slot("debug")
        dispatcher.utter_template("utter_toggle_debug", tracker, debug=debug)
        return [SlotSet("debug", debug)]


class ActionSearch(Action):
    STEP = 5
    def __init__(self):
        super().__init__()
        self.graph = Graph(grakn_url=Config.GRAPH_URL, es_url=Config.SEARCH_URL)

    def name(self):
        return 'action_search'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        dispatcher.utter_template("utter_search", tracker, query=self._humanize_search(tracker))
        search_fields = tracker.get_slot("search-fields")
        offset = 0 if search_fields != tracker.get_slot("last-search-fields") else tracker.get_slot("last-search-offset")

        results = self.graph.query(search_fields, limit=self.STEP, offset=offset)
        followup_action = "action_search_results" if results else "action_no_results"

        return [SlotSet("search-results", results),
                SlotSet("last-search-fields", search_fields),
                SlotSet("last-search-offset", offset+self.STEP),
                FollowupAction(followup_action)]

    def _humanize_search(self, tracker):
        preposition_map = {"person": "by",
                           "place": "of",
                           "concept": "related to"}
        human_text = ""
        search_fields = tracker.get_slot("search-fields")
        otype = search_fields["heritageObject"]["type"] if search_fields.get("heritageObject", {}).get("type") else "object"
        human_text += self._localize(self._pluralize(otype)) # Localize/translate
        for field,value in search_fields.get("heritageObject", {}).items():
            if field != "type":
                f = field if field != "*" else ""
                human_text += self._localize(" with " + f) + " " + value
        for oname, odict in search_fields.items():
            if oname in preposition_map:
                human_text += self._localize(" {} ".format(preposition_map[oname]) ) + str(odict.get("preferredLabel"))

        if tracker.get_slot("debug"):
            human_text += "({})".format(search_fields)
        return human_text

    def _carousel_results(self, results, debug=False):
        elements = []
        for result in results[:self.STEP]:
            h_object = result["heritageObject"]
            web_links = {wl["urlType"]: wl["url"] for wl in result.get("webLink", []) if wl.get("url")}
            element = dict()
            url = web_links.get("webpage")
            element["title"] = self._shorten_text(h_object.get("title", "N/A"))
            element["image_url"] = web_links.get("media", web_links.get("preview_source"))
            element["subtitle"] = self._shorten_text(h_object.get("description", "---"))
            if debug and result.get("debug"):
                element["subtitle"] = self._shorten_text(result.get("debug"))
            element["buttons"] = []
            if url:
                element["buttons"] = [
                  {
                    "type": "web_url",
                    "url": url,
                    "title": "View",
                  },
                  {
                    "type": "postback",
                    "payload": '/related{"relating-object-id": "' + h_object["providerId"] + '"}',
                    "title": "Related",
                  }
                ]
            elements.append(element)
        logging.info(json.dumps(elements))
        return elements

    # TODO: extract to utils
    @staticmethod
    def _shorten_text(text, limit=80):
        if len(text) <= limit:
            return text
        text = text[:limit-1]
        # Try shortening on whitespace
        last_space = text.rfind(' ')  # TODO: search other whitespaces too
        if limit - last_space < 20: # if not space found within reasonable string, just cut as is
            limit = last_space+1
        short =  text[:limit-1] + "…"
        logging.info("Shortening text to: {}, length: {}".format(short, len(short)))
        return short


    def _localize(self, text):
        # TODO: pass target language
        return Translator.translate(text, "en")

    def _pluralize(self, text):
        # TODO: use inflect library (though it is not perfect
        if text.endswith('s'): return text
        text += "s"
        return text

class ActionSearchResults(ActionSearch):
    def name(self):
        return 'action_search_results'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        otype = tracker.get_slot("gs_object_type")
        if not otype: otype = self._localize("object")
        results = tracker.get_slot("search-results")
        dispatcher.utter_template("utter_show_results", tracker, objects=self._localize(self._pluralize(otype)))
        dispatcher.utter_custom_message(*self._carousel_results(results))
        dispatcher.utter_template("utter_ask_show_more", tracker)
        #return [FollowupAction("action_listen")]
        return []

class ActionNoResults(ActionSearch):
    def name(self):
        return 'action_no_results'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        dispatcher.utter_template("utter_no_results", tracker)
        dispatcher.utter_template("utter_go_guided", tracker)
        #return [FollowupAction("action_listen")]
        return []


class ActionSearchRelated(ActionSearch):
    def name(self):
        return 'action_search_related'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        dispatcher.utter_template("utter_search_related", tracker)
        if tracker.get_slot("debug"):
            dispatcher.utter_message("Relating object: {}".format(tracker.get_slot("relating-object-id")))
        results = self.graph.related(tracker.get_slot("relating-object-id"))
        otype = tracker.get_slot("gs_object_type")
        if not otype: otype = self._localize("object")
        dispatcher.utter_template("utter_show_results", tracker, objects=self._localize(self._pluralize(otype)))
        dispatcher.utter_custom_message(*self._carousel_results(results, debug=tracker.get_slot("debug")))
        return [SlotSet("related-search-results", results)]

class ActionNotUnderstood(Action):
    def name(self):
        return 'action_not_understood'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):

        dispatcher.utter_template("utter_not_understood", tracker)
        return [SlotSet("guided-search", True)]

class ActionInitGuidedSearch(Action):
    def name(self):
        return 'action_init_guided_search'

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        dispatcher.utter_template("utter_ask_gs_object_type", tracker)
        slots_set = []
        # Clear all form fields
        for slot in list(FormActionObject.SEARCH_MAPPING.keys()) + ['search-fields']:
            slots_set.append(SlotSet(slot, None))
        return slots_set

class ActionStats(ActionSearch):
    def name(self):
        return "action_stats"

    def run(self, dispatcher: Dispatcher,
                  tracker: DialogueStateTracker,
                  domain: Domain):
        slots_set = []
        stats = self.graph.stats()
        org_number = stats["organisation"]["hits"]["total"]
        org_list = ",".join([o["_source"]["preferredLabel"] for o in stats["organisation"]["hits"]["hits"][:5]])
        dispatcher.utter_template("utter_organisation_stats", tracker, org_number=org_number, org_list=org_list)

        return slots_set



# Guided search - object type
class FormActionObjectType(FormAction):
    AUTHOR_MAPPING = {
        "book" : "author",
        "painting" : "painter",
        "photograph": "photographer"
    }

    def name(self):
        return "form_object_type"

    @staticmethod
    def required_slots(tracker):
        return ["gs_object_type"]

    def submit(self, dispatcher, tracker, domain):
        gs_object_type = tracker.get_slot("gs_object_type")
        author_type = "author"  # shouldn't happen but just in case
        if gs_object_type:
            author_type = self.AUTHOR_MAPPING.get(gs_object_type, author_type)
        return [SlotSet("gs_author_type", Translator.translate(author_type))]


# Guided search forms
class FormActionObject(FormAction):
    SEARCH_MAPPING = {"gs_object_type": ["heritageObject", "type"],
                      "gs_title": ["heritageObject", "title"],
                      "gs_author": ["person", "preferredLabel"]}

    def name(self):
        return "form_object"

    def submit(self, dispatcher, tracker, domain):
       # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict]
       """Define what the form has to do
           after all required slots are filled"""

       slots = []

       search_fields = defaultdict(defaultdict)
       prev_search_fields = tracker.get_slot("search-fields")
       if prev_search_fields:
           search_fields.update(prev_search_fields)
       for slot, slot_map_list in self.SEARCH_MAPPING.items():
           value = tracker.get_slot(slot)
           if isinstance(value, (list,tuple)):
               value = list(set(value))
               if len(value) == 1: value = value[0]
           # Update search fields to be: {"heritageObject": {"title" : "War and Peace"}}
           if value and value != "*":
               search_fields[slot_map_list[0]][slot_map_list[1]] = value

       if search_fields: slots += [SlotSet("search-fields", search_fields)]
       return slots + [FollowupAction("action_search")]

    def slot_mappings(self):
        # type: () -> Dict[Text: Union[Dict, List[Dict]]]
        return {"gs_author": [self.from_intent(intent="skip", value="*"),
                              self.from_entity(entity="gs_author"),
                              self.from_text()],
                "gs_title": [self.from_intent(intent="skip", value="*"),
                              self.from_text()]}

class FormActionBook(FormActionObject):
    def name(self):
        return "form_book"

    @staticmethod
    def required_slots(tracker):
        return ["gs_author", "gs_title"]


class FormActionPainting(FormActionObject):
    def name(self):
        return "form_painting"

    @staticmethod
    def required_slots(tracker):
        # type: () -> List[Text]
        return ["gs_author", "gs_title"]

class FormActionPhotograph(FormActionObject):
    def name(self):
        return "form_photograph"

    @staticmethod
    def required_slots(tracker):
        return ["gs_title"]


if __name__ == "__main__":
    import pprint
    action = ActionSearch()
    q = action._build_query({"heritageObject": {"title": "Gallery"}})
    pprint(q)


