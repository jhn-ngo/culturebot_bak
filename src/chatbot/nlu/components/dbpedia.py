from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from rasa_nlu.extractors import EntityExtractor
import logging
import re
from ner.engines import NERDbPedia

class DbpediaExtractor(EntityExtractor):
    """A new component"""

    # Name of the component to be used when integrating it in a
    # pipeline. E.g. ``[ComponentA, ComponentB]``
    # will be a proper pipeline definition where ``ComponentA``
    # is the name of the first component of the pipeline.
    name = "ner_dbpedia"

    # Defines what attributes the pipeline component will
    # provide when called. The listed attributes
    # should be set by the component on the message object
    # during test and train, e.g.
    # ```message.set("entities", [...])```
    provides = ["entities"]

    # Which attributes on a message are required by this
    # component. e.g. if requires contains "tokens", than a
    # previous component in the pipeline needs to have "tokens"
    # within the above described `provides` property.
    requires = []

    # Defines the default configuration parameters of a component
    # these values can be overwritten in the pipeline configuration
    # of the model. The component should choose sensible defaults
    # and should be able to create reasonable results with the defaults.
    defaults = {"url": "http://model.dbpedia-spotlight.org/{}/annotate",
                "confidence": 0.5}

    # Defines what language(s) this component can handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    language_list = None

    def __init__(self, component_config=None):
        super(DbpediaExtractor, self).__init__(component_config)
        self.dbpedia = NERDbPedia(url=self.component_config["url"])

    @classmethod
    def required_packages(cls):
        # type: () -> List[Text]
        return [] #["pyspotlight"]


    def train(self, training_data, cfg, **kwargs):
        """Train this component.

        This is the components chance to train itself provided
        with the training data. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.train`
        of components previous to this one."""
        pass

    def process(self, message, **kwargs):
        logging.info("DbpediaExtractor: extracting {}".format(message.text))
        dbpedia_entities = self.dbpedia.extract(message.text)
        extracted = self.dbpedia_to_rasa(dbpedia_entities)
        extracted = self.add_extractor_name(extracted)
        message.set("entities", message.get("entities", []) + extracted,
                    add_to_output=True)
        logging.info("DbpediaExtractor: extracted {}".format(message.as_dict()))

    def dbpedia_to_rasa(self, dbpedia_entities):
        entities = []
        for dbpedia_entity in dbpedia_entities:
            entity = dict()
            entity["value"] = dbpedia_entity["entity"]["surfaceForm"]
            entity["start"] = dbpedia_entity["entity"]["offset"]
            entity["end"] = entity["start"] + len(entity["value"])
            entity["entity"] = self.extract_entity_type(dbpedia_entity)
            entity["confidence"] = dbpedia_entity["entity"]["similarityScore"] # TODO: normalize?
            entities.append(entity)
        return entities

    @staticmethod
    def extract_entity_type(dbpedia_entity):
        db_type = dbpedia_entity["entity"]["types"]
        if not db_type:
            db_type = ",".join([e["type"]["value"] for e in dbpedia_entity["details"] if "type" in e])
        if re.search("organization", db_type, re.I):
            return "ORG"
        elif re.search("person", db_type, re.I):
            return "PERSON"
        elif re.search("location|place", db_type, re.I):
            return "LOC"
        else:
            return "FAC" # fallback, maps to concept

    def persist(self, model_dir):
        """Persist this component to disk for future loading."""

        pass

    @classmethod
    def load(cls, model_dir=None, model_metadata=None, cached_component=None,
             **kwargs):
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            component_config = model_metadata.for_component(cls.name)
            return cls(component_config)
