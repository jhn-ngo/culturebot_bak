from rasa_core.interpreter import NaturalLanguageInterpreter, RasaNLUInterpreter
from rasa_nlu.extractors.entity_synonyms import ENTITY_SYNONYMS_FILE_NAME
from copy import deepcopy
import logging
import simplejson, os
from ner.engines import NERDbPedia
from chatbot.nlu.stopwords import Stopwords


class CultureBotInterpreter(NaturalLanguageInterpreter):
    MAPPING = {
        "property-heritageObject-type" : "gs_object_type",
        "property-person-preferredLabel": "gs_author",
        "property-concept-preferredLabel": "gs_topic",
        "property-place-preferredLabel": "gs_place",
        "property-timeSpan-preferredLabel": "gs_timeperiod",
        "property-heritageObject-title": "gs_title",
    }
    NER_MAPPING = {
        "PERSON": "property-person-preferredLabel",
        "ORG": "property-concept-preferredLabel",  # TODO: change to property-organisation-preferredLabel
        "GPE": "property-place-preferredLabel",
        "LOC": "property-place-preferredLabel",
        "FAC": "property-concept-preferredLabel",
        "EVENT": "property-concept-preferredLabel",
        "NORP": "property-concept-preferredLabel",
        "LAW": "property-concept-preferredLabel",
        "PRODUCT": "property-concept-preferredLabel",
        "LANGUAGE": "property-heritageObject-objectLanguage",
    }

    SYNONYMS = dict()

    def __init__(self, model_path="models/nlu/current"):
        self.rasa_nlu = RasaNLUInterpreter(model_path)
        self.dbpedia = NERDbPedia()
        self._load_synonyms(model_path)
        self._load_stopwords(model_path)

    def parse(self, text):
        rasa_res = self.rasa_nlu.parse(text)
        if "intent" not in rasa_res or not rasa_res["intent"]: rasa_res["intent"] = dict()
        intent_confidence = rasa_res["intent"].get("confidence", 0)
        logging.info("Rasa NLU results: {}".format(rasa_res))
        if float(intent_confidence) < 0.2:
            default_intent = "search" if len(rasa_res["entities"]) > 0 else "default"
            rasa_res["intent"] = {
                "name": default_intent,
                "confidence": 0.6
            }
            rasa_res["intent_ranking"] = [rasa_res["intent"]]
        rasa_res["entities"] = self._process_entities(rasa_res["entities"])
        rasa_res["entities"] = self._process_fallback_search(text, rasa_res["intent"], rasa_res["entities"])

        logging.info("RasaInterpreter: {}".format(rasa_res))
        return rasa_res

    def _process_entities(self, entities):
        oentities = []
        search_fields = dict()
        for e in entities:
            # Pre-process external entities
            if e["entity"] in self.NER_MAPPING:
                oe = deepcopy(e)
                oe["entity"] = self.NER_MAPPING[e["entity"]]
                e = oe

            if e["entity"] == "property-heritageObject-type":
                e["value"] = self._synonymize(e["value"])
            if e["entity"].startswith("property-"):
                p, obj, p_name = e["entity"].split("-")
                if not obj in search_fields: search_fields[obj] = dict()
                search_fields[obj][p_name] = e["value"]
                if e["entity"] in self.MAPPING:
                    oe = deepcopy(e)
                    oe["entity"] = self.MAPPING[e["entity"]]
                    oentities.append(oe)
            else:
                oentities.append(e)
        if search_fields:
            oentities.append({
                "entity": "search-fields",
                "value" : search_fields

            })
        return oentities

    def _process_fallback_search(self, text, intent, entities):
        logging.info("Entities before fallback: {}".format(entities))
        # only relevant for search intent
        if intent["name"] != "search": return entities
        # extract search-fields (if detected)
        search_fields = [e for e in entities if e["entity"] == "search-fields"]
        search_fields_value = None
        if search_fields:
            if len(search_fields[0]["value"].keys()) > 1 or "heritageObject" not in search_fields[0]["value"]:
                return entities
            search_fields_value = search_fields[0]["value"]
        # extract meaningful words and put them into the query
        doc = self.nlp(text)
        tokens = [t.text for t in doc if not t.is_stop]
        if not tokens: return entities
        if not search_fields_value:
            search_fields_value = {"heritageObject": dict()}
        search_fields_value["heritageObject"]["*"] = " ".join(tokens)
        entities.append({
            "entity": "search-fields",
            "value": search_fields_value

        })
        return entities

    def _load_stopwords(self, path):
        self.stopwords = Stopwords(path)
        self.nlp = self.stopwords.nlp # get spacy model tuned with loaded stopwords

    def _load_synonyms(self, path):
        self.SYNONYMS = simplejson.load(open(os.path.join(path, ENTITY_SYNONYMS_FILE_NAME)))
        logging.info("SYNONYMS: {}".format(self.SYNONYMS))

    def _synonymize(self, word):
        return self.SYNONYMS.get(word.lower(), word)

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    from pprint import pprint

    import sys
    w = CultureBotInterpreter(model_path="../models/nlu/current")
    pprint(w.parse(sys.argv[1]))
