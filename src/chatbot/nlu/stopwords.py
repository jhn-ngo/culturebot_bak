import os
import spacy
import logging
from spacy.tokenizer import Tokenizer

class Stopwords:
    def __init__(self, model_path, lang="en"):
        self.nlp = spacy.load(lang)
        self.tokenizer = Tokenizer(self.nlp.vocab)
        self._load_from_smalltalk(os.path.join(model_path, '..', '..', "nlu_training", "md", "smalltalk"))
        self._load_custom()

    def _load_from_smalltalk(self, smalltalk_path):
        for filename in os.listdir(smalltalk_path):
            if not filename.endswith(".md"): continue
            logging.info("Loading stopwords from smalltalk {}".format(filename))
            with open(os.path.join(smalltalk_path, filename), 'r') as f:
                for line in f:
                    if not line.strip().startswith('-'): continue
                    doc = self.tokenizer(line.lstrip('-'))# self.nlp(line.lstrip('-')) # todo: use tokenizer
                    for w in doc:
                        if not w.text.strip(): continue
                        self.nlp.vocab[w.text].is_stop = True

    def _load_custom(self):
        # TODO: load custom stopwords from file
        stopwords = ['I', 'looking', 'searching', 'interested', 'search']
        for w in stopwords:
            self.nlp.vocab[w].is_stop = True



if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    sw = Stopwords()
