import logging
import argparse
from rasa_core.tracker_store import MongoTrackerStore
from rasa_core.agent import Agent
from rasa_core.utils import EndpointConfig
from chatbot.nlu.interpreter import CultureBotInterpreter

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)


class CultureChatbot:
    ACTIONS_PORT=5077
    def __init__(self):
        self.interpeter = CultureBotInterpreter()
        #self.actions_server = subprocess.Popen("python3 -m rasa_core_sdk.endpoint --actions actions -p {}".format(self.ACTIONS_PORT).split())
        action_endpoint = EndpointConfig("http://localhost:{}/webhook".format(self.ACTIONS_PORT)) # TODO : read from endpoints.yml

        self.rasa_agent = Agent.load("models/dialogue",
                           interpreter=self.interpeter,
                           action_endpoint=action_endpoint,
                           tracker_store=MongoTrackerStore(None, db="culturebot"))

    def __call__(self, channel, **kwargs):
        from rasa_core.run import serve_application
        args = {"credentials_file" : kwargs["credentials"] if "credentials" in kwargs else None}
        if "port" in kwargs: args["port"] = kwargs["port"]

        serve_application(self.rasa_agent,
                          channel,
                          **args)

    # For testing purposes
    def handle_message(self, message, sender_id):
        return self.rasa_agent.handle_message(message, sender_id)

def parse_arguments():
    """Parse all the command line arguments for the run script."""
    parser = argparse.ArgumentParser(
        description='starts the bot')
    parser.add_argument(
        '-p', '--port',
        default=6002,
        type=int,
        help="port to run the server at (if a server is run "
             "- depends on the chosen channel, e.g. facebook uses this)")
    parser.add_argument(
        '--credentials',
        default=None,
        help="authentication credentials for the channel as a yml file")
    parser.add_argument(
        '-c', '--channel',
        default="cmdline",
        choices=["facebook", "cmdline"],
        help="channel to connect to")


    return parser.parse_args()

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    args = parse_arguments()
    bot = CultureChatbot()
    bot(args.channel, credentials=args.credentials, port=args.port)
