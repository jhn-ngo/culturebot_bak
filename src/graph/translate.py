import logging

class Translator:
    def __init__(self, search):
        self.search = search

    def translate(self, object, slang, tlang):
        # TODO
        return object

    # Complete object with missing fields which possible found in the given source (or any other) translation
    def complement(self, index, object, slang=None, tlang=None):
        res = self.search.query(index, {"providerId": object["providerId"]})
        es_object_dict = res["hits"]["hits"][0]["_source"]
        if not "_translations" in es_object_dict: return object
        translations = es_object_dict["_translations"]
        if not slang or slang in translations:
            if not slang: slang = [k for k in translations][0] # pick any first translation
            # Complete missing fields
            for key in translations[slang]:
                if key not in object: object[key] = translations[slang][key] # TODO: automatically translate into tlang
        return object

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    from graph.search import GraphSearch
    gs = GraphSearch("https://bot.jewisheritage.net:443/es")
    tr = Translator(gs)
    print(tr.complement("heritageobject", {"title": "Kuku", "providerId" : "heritageObject:europeana:/2048008/Athena_Plus_ProvidedCHO_Ajuntament_de_Girona_553891"}))
