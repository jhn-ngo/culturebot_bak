import re
from ner.engines import NERDbPedia
from collections import defaultdict

class SemanticEnricher:
    def __init__(self):
        self.dbpedia = NERDbPedia()

    def enrich(self, text, language="en"):
        entities = []
        if language != "en": return entities
        dbpedia_out = self.dbpedia.extract(text)
        return self.dbpedia_to_graph(dbpedia_out)

    def dbpedia_to_graph(self, dbpedia_entities):
        nodes = defaultdict(list)
        provider_ids = set()
        for dbpedia_entity in dbpedia_entities:
            entity = dict()
            entity["providerId"] = "dbpedia:{}".format(dbpedia_entity["entity"]["URI"])
            if entity["providerId"] in provider_ids:
               continue
            provider_ids.add(entity["providerId"])
            entity["preferredLabel"] = dbpedia_entity["entity"]["surfaceForm"]
            node_type,entity["_type"] = self.extract_entity_type(dbpedia_entity)
            nodes[node_type].append(entity)
        return nodes

    def extract_entity_type(self, dbpedia_entity):
        db_type = dbpedia_entity["entity"]["types"]
        if not db_type:
            db_type = ",".join([e["type"]["value"] for e in dbpedia_entity["details"] if "type" in e])
        if re.search("organization", db_type, re.I):
            return "agents","organization"
        elif re.search("person", db_type, re.I):
            return "agents","person"
        elif re.search("location|place", db_type, re.I):
            return "places","place"
        else:
            return "concepts","concept" # fallback, maps to concept


if __name__ == "__main__":
    import sys
    se = SemanticEnricher()
    print(se.enrich(sys.argv[1]))
