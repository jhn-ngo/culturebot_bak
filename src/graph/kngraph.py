import grakn
import random
import logging
import re
import lmdb
from graph.search import GraphSearch
from collections import namedtuple

from graph.query.insert import InsertQuery
from graph.query.match import MatchQuery
from graph.query.related import RelatedQuery
from graph.query.exists import ExistsQuery
from graph.query.hobject_weblinks import HObjectWeblinkQuery
from graph.translate import Translator
VariableObject = namedtuple('VariableObject', ('variable', 'object'))

class Graph:
    KEYSPACE = "culturebot"

    def __init__(self, grakn_url = "localhost:48555", es_url="localhost:9200"):
        self.session = None
        self.transaction = None
        self.grakn = grakn.Grakn(grakn_url)
        self.search = GraphSearch(es_url)
        self.translator = Translator(self.search)
        self.id_db = lmdb.Environment("/var/culturebot", map_size=int(5e9)) # TODO: extract to Config, use 5G for now

    def __del__(self):
        self.commit()
        #self._close_session()

    def commit(self):
        if self.transaction:
            logging.info("Committing transaction: {}".format(self.transaction))
            self.transaction.commit()
        self.transaction = None
        self.search.commit()


    def insert(self, row):
        self.transaction = self._get_transaction(grakn.TxType.WRITE)
        return InsertQuery(self.search, self.transaction, self.id_db)(row)

    def query(self, q, limit=10, offset=0, complement=True):
        transaction = self._get_transaction(grakn.TxType.READ)
        results = MatchQuery(self.search, transaction, self.id_db)(q, limit, offset)
        # Complement fields from other languages (if exist)
        if results and complement:
            for res in results:
                res["heritageObject"] = self.translator.complement("heritageObject", res["heritageObject"])
        return results

    def related(self, provider_id, limit=5):
        transaction = self._get_transaction(grakn.TxType.READ)
        return RelatedQuery(self.search, transaction, self.id_db)(provider_id, limit)

    # Get all weblinks associated with the object
    def hobject_weblinks(self, ho_graph_id):
        transaction = self._get_transaction(grakn.TxType.READ)
        return HObjectWeblinkQuery(self.search, transaction, self.id_db)(ho_graph_id)

    def stats(self):
        # TODO: more elaborate stats
        return self.search.stats()

    def clean_ids(self, limit, skip):
        keys_to_clean = set()
        # Collect IDs to remove
        with self.id_db.begin(write=False) as txn:
            cursor = txn.cursor()
            i = 0
            i_s = 0
            for provider_id, graph_id in cursor:
                logging.info("Checked {} ids".format(i))
                i_s += 1
                if skip and i_s < skip: continue
                i += 1
                if limit and i > limit: break
                exists_query = ExistsQuery(self.search, self._get_transaction(grakn.TxType.READ)) 
                if not exists_query(graph_id.decode()):
                    logging.info("Key-value to clean: {} -> {}".format(provider_id,graph_id))
                    keys_to_clean.add(provider_id)
        # Actual removal 
        logging.info("About to delete {} IDs".format(len(keys_to_clean)))
        with self.id_db.begin(write=True) as txn:
            for id in keys_to_clean:
                logging.info("Deleting {}".format(id))
                txn.delete(id)

    def _get_session(self):
        if not self.session:
            self.session = self.grakn.session(self.KEYSPACE)
        return self.session

    def _get_transaction(self, type: grakn.TxType):
        if self.transaction:
            return self.transaction
        session = self._get_session()
        self.transaction = session.transaction(type)
        return self.transaction

    def _close_session(self):
        if not self.session: return
        self.session.close()
        self.session = None


if __name__ == "__main__":
    import pprint, logging
    from chatbot.config import Config
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    graph = Graph(grakn_url=Config.GRAPH_URL, es_url=Config.SEARCH_URL)
    #q = 'match $x isa heritageObject, has title $y; {$y contains "Amsterdam";} or {$y contains "Netherlands";}; get;'
    queries = [{"heritageObject": {"type" : "painting"},
                "person": {"preferredLabel": "rembrandt"}},
                {"heritageObject": {"type" : "painting",
                                  "title": "Night"}}]
    q1 = [{"heritageObject": {"type" : "painting"}}]
    #for q in q1:#queries:
    #    for r in graph.query(q, limit=10):
    #        pprint.pprint(r)
    print("RELATED OBJECTS: ", graph.related("europeana:/90402/SK_A_4833", limit=2))
    graph.commit()
    # client = grakn.Grakn(uri = "localhost:48555")
    # with client.session(keyspace = Graph.KEYSPACE) as session:
    #     with session.transaction(grakn.TxType.READ) as tx:
    #         print([r for r in tx.query(q)])
    #         tx.commit()
