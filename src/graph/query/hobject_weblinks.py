from graph.query.base import BaseQuery
import logging


class HObjectWeblinkQuery(BaseQuery):

    def __call__(self, ho_graph_id):
        gql_query = self._query_weblinks(ho_graph_id)
        logging.info("Weblinks query: {}".format(gql_query))
        concept_iterator = self.transaction.query(gql_query)
        weblinks = []
        for concept_map in concept_iterator:
            weblink = self._c2d(concept_map.get('webLink'))
            weblinks.append({"webLink": weblink})
        return weblinks

    def _query_weblinks(self, ho_graph_id):
        gql_query = 'match $webLink isa webLink; ' \
                    '$heritageObject isa heritageObject id {}; ' \
                    '($heritageObject, $webLink);'.format(ho_graph_id)
        return gql_query + " get; "

    # todo: avoid duplicating function
    def _c2d(self, concept):
        d = {"graphId": concept.id}
        for attr in concept.attributes():
            d[attr.type().label()] = attr.value()
        return d
