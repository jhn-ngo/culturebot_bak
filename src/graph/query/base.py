
from utils.timer import Timer
from graph.enricher import SemanticEnricher

class BaseQuery:
    def __init__(self, search, transaction, id_db=None):
        self.search = search
        self.transaction = transaction
        self.id_db = id_db
        self.timer = Timer(self.__class__.__name__)
        self.enricher = SemanticEnricher()

    def __call__(self, *args, **kwargs):
        raise Exception("Child classes should implement this method")


    def _concept_to_dict(self, concept_map, types):
        if not isinstance(types, (list, tuple)):
            types = [types]
        d = dict()
        for key in types:
            if key not in concept_map: continue
            d[key] = {"id": concept_map[key].id}
            for attr in concept_map[key].attributes():
                d[key][attr.type().label()] = attr.value()

        return d
