from graph.query.base import BaseQuery
from graph.query.hobject_weblinks import HObjectWeblinkQuery
import logging


class RelatedQuery(BaseQuery):

    def __call__(self, hobject_provider_id, limit):
        out_related_objects = []
        # TODO: smarted split by relation
        out_related_objects += self._related_by_title("heritageObject", hobject_provider_id, 2)

        for otype,olimit in [("person",2), ("timeSpan", 2), ("concept", 2)]:
            out_related_objects += self._related_by(otype, hobject_provider_id, olimit)
        return out_related_objects[:limit]

    def _related_by(self, otype, hobject_provider_id, limit):
        gql_query = 'match $heritageObject isa heritageObject has providerId "{}"; ' \
                    '$r isa {} has preferredLabel $rpl; ($heritageObject, $r); ' \
                    '$heritageObjectRelated isa heritageObject; ' \
                    '($heritageObjectRelated, $r); get;'.format(hobject_provider_id, otype) # we always interested in heritageObject
        logging.info("Searching related by {}, query: {}".format(otype, gql_query))
        concept_iterator = self.transaction.query(gql_query)
        related_objects = []
        for concept_map in concept_iterator:
            related_object = self._c2d(concept_map.get('heritageObjectRelated'))
            relation_object =  self._c2d(concept_map.get('r'))
            if related_object["providerId"] != hobject_provider_id:
                related_objects.append({"heritageObject" : related_object,
                                        "webLink": self._get_weblinks(related_object["graphId"]),
                                        "debug": "Related by {}, label: {}".format(otype, relation_object.get("preferredLabel"))})
            if len(related_objects) >= limit: break
        return related_objects

    def _related_by_title(self, otype, hobject_provider_id, limit):
        objects = [o for o in self.search.query(otype, {"providerId": hobject_provider_id})]
        if not objects: return []
        title = objects[0]["title"] if "title" in objects[0] else None
        if not title: return []
        related_objects = self.search.more_like_this(otype, title)
        out_related_objects = []
        for related_object in related_objects:
            if related_object["providerId"] != hobject_provider_id:
                out_related_objects.append({"heritageObject" : related_object,
                                            "webLink": self._get_weblinks(related_object["graphId"]),
                                            "debug": "Related by title: {}".format(title)})
            if len(out_related_objects) >= limit: break
        return out_related_objects

    def _get_weblinks(self, graph_id):
        weblinks = HObjectWeblinkQuery(self.search, self.transaction, self.id_db)(graph_id)
        return [weblink_dict["webLink"] for weblink_dict in weblinks]

    def _c2d(self, concept):
        d = {"graphId": concept.id}
        for attr in concept.attributes():
            d[attr.type().label()] = attr.value()
        return d
