from graph.query.base import BaseQuery
import logging

class ExistsQuery(BaseQuery):

    def __call__(self, graph_id):
        gql_query = "match $o id {}; get;".format(graph_id)
        concept_iterator = self.transaction.query(gql_query)
        for concept in concept_iterator:
            concept_dict = self._concept_to_dict(concept.map(), "o")
            if concept_dict: return True
        return False
