import random
import string
from collections import namedtuple
from graph.query.base import BaseQuery
import lmdb
import logging

VariableObject = namedtuple('VariableObject', ('variable', 'object'))
SearchObject = namedtuple('SearchObject', ('concrete_type', 'object'))

class InsertQuery(BaseQuery):
    def __call__(self, row):
        self.search_objects = dict()
        self.timer.start("build_query")
        objVars,query = self._insert(row)
        self.timer.stop("build_query")
        #logging.info("Insert row query: {}".format(query))
        self.timer.start("execute_query")
        insert_iter = self.transaction.query(query)
        self.timer.stop("execute_query")

        provider2graph = dict()
        self.timer.start("collect_concepts")
        concepts = insert_iter.collect_concepts()
        self.timer.stop("collect_concepts")
        for c in concepts:
            providerId = [attr.value() for attr in c.attributes() if attr.type().label() == "providerId"]
            if providerId: provider2graph[providerId[0]] = c.id
        print("Concepts: {}, provider2graph: {}".format(concepts, provider2graph))
        # Insert searh object into search index
        for providerId, so in self.search_objects.items():
            so.object["graphId"] = provider2graph.get(so.object.get("providerId"))
            self.timer.start("write_lmdb")
            if not self._provider_id2id(so.concrete_type, so.object.get("providerId")):                
                with self.id_db.begin(write=True) as txn:
                   try:
                      txn.put(providerId.encode(), so.object["graphId"].encode(), dupdata=False, overwrite=False) 
                   except:
                      logging.error("Failed to put LMDB: {} -> {}".format(providerId, so.object["graphId"]))

            self.timer.stop("write_lmdb")
 
            print("Inserting object into search index: {}".format(so.object))
            self.timer.start("write_es")
            self.search.insert(so.concrete_type, so.object)
            self.timer.stop("write_es")

    def _insert(self, row, update=False):
        # Insert heritage object
        insert_query = "insert\n"
        # Insert other objects
        objVars = dict()
        objectTypes = ["agent", "place", "webLink", "timeSpan", "concept"]
        objectLanguage = row["heritageObject"].get("objectLanguage", "en")
        # Semantically enrich heritage object
        row = self._enrich_object(row, objectLanguage)
        for otype in ["heritageObject"] + objectTypes:
            objs = row.get(otype, row.get("{}s".format(otype)))
            if not isinstance(objs, list): objs = [objs]
            objVars[otype] = []
            for obj in objs:
                if not obj: continue
                objVar,oquery  = self._get_insert_object(obj, otype, objectLanguage)
                objVars[otype].append(VariableObject(variable=objVar,object=obj))
                insert_query += oquery
        # Insert data provider
        insert_query += self._insert_data_provider(objVars.get("webLink"), objectLanguage)
        # Insert relations
        heritageObjectVar = objVars["heritageObject"][0].variable
        for otype in objectTypes:
            for objVar in objVars.get(otype, []):
                relType = "object{}".format(self._capitalize(otype))
                relVar = "${}{}".format(relType, self._randint())
                insert_query += "{} (object: {}, object{}: {}) isa {};\n".format(relVar, heritageObjectVar, self._capitalize(otype), objVar.variable, self._concretize_relation_type(otype))
        # Prepend match clause to execute match-insert to avoid inserting duplicated items
        if update:
            match_query = "match "
            for otype in ["heritageObject"] + objectTypes:
                for objVar in objVars.get(otype, []):
                    match_query += '{} has providerId "{}"; '.format(objVar.variable, objVar.object["providerId"])
        else:
            match_query = ""
        # TODO: insert webLink -> dataProvider relationship
        print("QUERY: {}".format(insert_query))
        return objVars,match_query + insert_query

    def _enrich_object(self, row, language):
        hobject = row["heritageObject"]
        hobject.update(self._select_translation(hobject, language))  
        print("Enriching object: {}".format(hobject))        
        nodes_dict = self.enricher.enrich(hobject.get("title", ""), language)
        if nodes_dict:
            for node_type, nodes in nodes_dict.items():
                if node_type not in row: row[node_type] = []
                row[node_type] += nodes
            print("Enriched with {}".format(nodes_dict))
        return row

    def _insert_data_provider(self, var_objs, lang):
        insert_query = ""
        for var_obj in var_objs:
            orgVar, orgQuery = self._get_insert_organisation(var_obj.object, lang)
            if not orgVar: continue
            insert_query += orgQuery
            relVar = "${}{}".format("dataProvider", self._randint())
            insert_query += "{} (webLinkProvided: {}, webLinkProvider: {}) isa dataProvider; ".format(relVar, var_obj.variable, orgVar)
        return insert_query

    def _get_insert_organisation(self, webLink, lang):
        data_provider = webLink.get("_dataProvider")
        if not data_provider: return None,None
        orgVar,orgQuery = self._get_insert_object(data_provider, "organisation", lang)
        return orgVar,orgQuery

    def _get_insert_object(self, obj, otype, olanguage):
        concrete_otype = self._concretize_type(obj, otype)
        objVar = "${}{}".format(concrete_otype, self._randint())

        query = "{} isa {}".format(objVar, concrete_otype)
        # If object already exists
        id = self._provider_id2id(concrete_otype, obj.get("providerId"))
        if id:
            query += " id {};\n".format(id)
            return objVar,query 
        # Insert selected translation into the object itself
        obj.update(self._select_translation(obj, olanguage))
        # Insert object into search index (if it is searchable)
        print(concrete_otype)
        if self.search.is_searchable(concrete_otype):
            print("Collecting object into the search index")
            self.search_objects[obj.get("providerId")] = SearchObject(concrete_otype, obj)

        for key, value in obj.items():
            if key.startswith("_") or not value: continue
            query += ' has {} {}'.format(key, self._format_value(key, value))
        query += ";\n"
        return objVar,query

    def _provider_id2id(self, otype, provider_id):
        if not self.search.is_searchable(otype): return None
        if not provider_id: return None
        self.timer.start("- read_lmdb")
        with self.id_db.begin(write=False) as txn:
            id = txn.get(provider_id.encode())
            if id: return id.decode()
        self.timer.stop("- read_lmdb")
               
        #res = self.search.query(otype, {"providerId": provider_id})
        #for hit in res.hits:
        #   print(hit._d_)
        #   if "graph_id" in hit:
        #      return hit["graph_id"]
        return None

    def _format_value(self, attr, value):
        # TODO: avoid hardcoded, use Graql schema instead
        if attr in ["latitude", "longitude", "altitude"]:
            return value
        # Default - string value
        value = str(value).replace('"', "'").replace("\n", "\\n")
        return '"{}"'.format(value)

    def _concretize_type(self, obj, otype):
        # For now only agents have concrete type (person or organisation
        if otype != "agent": return otype
        # TODO: Heuristics to detect persons and organisation, for now all agents are persons
        return "person"

    def _concretize_relation_type(self, otype):
        # TODO: support more
        if otype == "concept": return "topic"
        return "o" + self._capitalize(otype)

    def _select_translation(self, object, language):
        translations = object.get("_translations")
        if not translations: return dict()
        # For now, prefer English and
        if "en" in translations: return translations["en"]
        if language in translations: return translations[language]
        # no english or objectLanguage translation found
        return dict()

    def _randint(self):
        return ''.join(random.choice(string.ascii_uppercase) for x in range(6))

    def _capitalize(self, s):
        return s[:1].upper() + s[1:]

