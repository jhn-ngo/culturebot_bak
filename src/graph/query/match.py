import re
from graph.query.base import BaseQuery
import logging

class MatchQuery(BaseQuery):

    def __call__(self, q, limit=10, offset=0):
        hobjects = self._match(q, limit, offset)
        if len(hobjects) < limit and offset == 0:
            self._lazy_load(q)
        return self._match(q, limit, offset)

    def _match(self, q, limit, offset):
        if q: q = q.copy()
        hobjects = []
        hobject_ids = set()
        # 1. Query heritageObjects
        gql_queries = self._build_object_query_by_id(q, limit, offset)
        for gql_query in gql_queries:
            logging.info("Object query: {}".format(gql_query))
            if not gql_query: return []
            concept_iterator = self.transaction.query(gql_query)
            for concept in concept_iterator:
                concept_dict = self._concept_to_dict(concept.map(), "heritageObject")
                if concept_dict["heritageObject"]["id"] not in hobject_ids:
                    hobjects.append(concept_dict)
                    hobject_ids.add(concept_dict["heritageObject"]["id"])
        # 2. For each HO, query its web links
        offset_hobjects = hobjects #hobjects[offset:offset+limit]
        for hobject in offset_hobjects:
            gql_query = self._build_weblink_query(hobject["heritageObject"]["id"])
            hobject["webLink"] = list()
            logging.info("Weblink query: {}".format(gql_query))
            concept_iterator = self.transaction.query(gql_query)
            for concept in concept_iterator:
                hobject["webLink"].append(self._concept_to_dict(concept.map(), "webLink")["webLink"])
        # TODO: cache previous
        return offset_hobjects

    def _lazy_load(self, q):
        # TODO: load graph lazily
        from etl.europeana.lazy_load import EuropeanaLazyLoad
        ll_query = " ".join([v for k,f_dict in q.items() for f,v in f_dict.items() if f != "type"])
        type = q.get("heritageObject", dict()).get("type")
        qf = "what:{}".format(type) if type else None
        logging.info("Building lazy load query: '{}' from structure: {}".format(ll_query, q))
        EuropeanaLazyLoad()(query=ll_query, qf=qf, limit=10)
        # TODO: run separate job with remaining objects of the query

    def _build_object_query_by_id(self, query_struct, limit, offset):
        # Search object in ES if title was specified
        if "heritageObject" in query_struct and "title" in query_struct["heritageObject"]:
            hobjects_found = self._search_objects("heritageObject", query_struct["heritageObject"])
        else:
            hobjects_found = [{"graphId": None}]
        gql_queries = []
        for hobject in hobjects_found:
            gql_query = "match $heritageObject isa heritageObject".format(hobject["graphId"])
            gql_query += " id {} ".format(hobject["graphId"]) if hobject["graphId"] else ""
            if "heritageObject" in query_struct:
                has_query = self._build_has_query_fragment("heritageObject", query_struct["heritageObject"], exclude_fields=['title'])
            else:
                has_query = ""
            gql_query += has_query if has_query else ";"
            # Enhance query by relations
            rel_query = ""
            if query_struct:
                for object, fields_dict in query_struct.items():
                    if object == "heritageObject": continue
                    rel_query = "($heritageObject, ${}) isa {}; ${}".format(object, self._object2relation(object), object)
                    has_query = self._build_has_query_fragment(object, fields_dict)
                    if not has_query: return []
                    rel_query += has_query
            if not gql_query.strip().endswith(";"): gql_query += "; "
            gql_query += rel_query
            if not gql_query.strip().endswith(";"): gql_query += "; "
            gql_query += " offset {}; limit {}; get;".format(offset, limit)
            gql_queries.append(gql_query)
        return gql_queries



    def _build_object_query(self, query_struct, limit, offset):
        gql_query = "match $heritageObject isa heritageObject" # we always interested in heritageObject
        if query_struct and "heritageObject" in query_struct:
            has_query = self._build_has_query_fragment("heritageObject", query_struct["heritageObject"])
            if not has_query: return None
            gql_query += has_query
            del query_struct["heritageObject"]
        # Enhance query by relations
        rel_query = ""
        if query_struct:
            for object, fields_dict in query_struct.items():
                rel_query = "($heritageObject, ${}) isa {}; ${}".format(object, self._object2relation(object), object)
                has_query = self._build_has_query_fragment(object, fields_dict)
                if not has_query: return None
                rel_query += has_query
        if not gql_query.strip().endswith(";"): gql_query += "; "
        gql_query += rel_query
        if not gql_query.strip().endswith(";"): gql_query += "; "
        gql_query += "  offset {}; limit {}; get;".format(offset, limit)
        return [gql_query]

    def _build_has_query_fragment(self, otype, query_struct, exclude_fields = ()):
        # First, make free text search for the objects of the given type
        hobjects_found = self._search_objects(otype, query_struct, exclude_fields)
        # No object found by free text -> return empty query
        if not hobjects_found: return None
        # Now, build Grakn query to retrieve actual objects by matched field values
        contains_query = ""
        gql_query = ""
        for field, value in query_struct.items():
            if field in exclude_fields: continue
            if field == "*": field = "providerId" # free-text search - match graph by id
            gql_query += ' has {} ${} '.format(field, field)
            contains_query += " or ".join(
                list(set(['{{${} {};}}'.format(field, self._format_value(field, hobject[field])) for hobject in hobjects_found if field in hobject]))) + "; "
        gql_query += "; " + contains_query
        return gql_query

    def _search_objects(self, otype, query_struct, exclude_fields=()):
        return [o for o in self.search.query(otype, query_struct, exclude_fields)]

    def _format_value(self, field, value):
        value = re.sub('(")', r"\\\1", value)
        # TODO: clarify with Grakn support
        # Due to performance issues, the search-heavy fields values are matched by string equal
        if field in ["preferredLabel", "title", "type", "providerId"]:
            return '"{}"'.format(value)
        # Others, by regex
        return '/{}/'.format(value)

    def _object2relation(self, object):
        if object != "person": return "o" + object.capitalize()
        return "oAgent"

    def _build_weblink_query(self, hobject_id):
        gql_query = "match $webLink isa webLink; $heritageObject id {}; ".format(hobject_id)
        weblink_query = "($heritageObject, $webLink) isa oWebLink; ".format(hobject_id)
        gql_query +=  weblink_query + " get;"
        return gql_query
