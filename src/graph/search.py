from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text, Search
from elasticsearch_dsl.query import MoreLikeThis
from elasticsearch.helpers import bulk
import logging

#connections.create_connection(hosts=[{"host": "localhost", "port": 9201}])


class heritageObject(Document):
    graphId = Keyword()
    providerId = Keyword()
    type = Keyword()
    title = Text()
    description = Text()
    created = Text()

    class Index:
        name = 'heritageobject'


class person(Document):
    graphId = Keyword()
    providerId = Keyword()
    preferredLabel = Text()
    alternativeLabel = Text()
    dateOfBirth = Text()
    dateOfDeath = Text()
    class Index:
        name = 'person'


class organisation(Document):
    graphId = Keyword()
    providerId = Keyword()
    preferredLabel = Text()
    alternativeLabel = Text()

    class Index:
        name = 'organisation'

class place(Document):
    graphId = Keyword()
    providerId = Keyword()
    preferredLabel = Text()
    alternativeLabel = Text()

    class Index:
        name = 'place'

class concept(Document):
    graphId = Keyword()
    providerId = Keyword()
    preferredLabel = Text()
    alternativeLabel = Text()

    class Index:
        name = 'concept'


class timeSpan(Document):
    graphId = Keyword()
    providerId = Keyword()

    class Index:
        name = 'timespan'

class webLink(Document):
    graphId = Keyword()
    providerId = Keyword()

    class Index:
        name = 'weblink'



class GraphSearch:
    def __init__(self, url):
        # Map index name to Document-inherited class
        connections.create_connection(hosts=[url])
        for cl in Document.__subclasses__(): 
           print(cl)
           cl.init()
        self.index_map = {getattr(cl, "_index")._name: cl for cl in Document.__subclasses__()}
        self.docs = []

    def query(self, index, struct_query, exclude_fields=()):
        logging.info("GrapSearch::query: index {}, structured query: {}".format(index, struct_query))
        s = Search(index=index.lower())
        for k,v in struct_query.items():
            if k in exclude_fields: continue
            if k == "*":
                s = s.query('multi_match', **{"query": v}) # TODO: query specific fields with weights
            else:
                s = s.query('match', **{k:v})
        return s.execute()

    def insert(self, index: str, object: dict):
        cl = self.index_map.get(index.lower())
        cl_obj = cl(**object)
        cl_obj.meta.id = object.get("providerId")
        self.docs.append(cl_obj)

    def more_like_this(self, index: str, text: str):
        s = Search(index=index.lower())
        # TODO: enforce same type by providing like= {"doc": {"title" : ..., "type" : type }
        s = s.query(MoreLikeThis(like=text, fields=['title'], min_term_freq=1, max_query_terms=12))
        hits = s.execute().hits
        return [h.to_dict() for h in hits]

    def commit(self):
        out = bulk(connections.get_connection(), (d.to_dict(True) for d in self.docs))
        logging.info("Committed {} docs to ES".format(len(self.docs)))
        self.docs.clear()

    def is_searchable(self, object: str):
        return object.lower() in self.index_map

    def init(self):
        for d in [heritageObject, person, place]:
            d.init()

    def stats(self):
        stats = dict()
        for index in self.index_map.keys():
            s = Search(index=index.lower())
            s.query('match_all')
            stats[index] = s.execute()
        return stats
if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)

    gs = GraphSearch("https://bot.jewisheritage.net:443/es")
    gs.init()
    gs.query("heritageobject", {"providerId" : "europeana:/90402/SK_NM_16709_2"})
    print(gs.stats())