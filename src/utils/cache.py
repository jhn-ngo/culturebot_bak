
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
import uuid
import json

class NERCache:
    INDEX = "mtcache"
    def __init__(self):
        self.es = Elasticsearch()
        try:
          self.es.indices.create(self.INDEX)
        except:
          pass


    def get(self, type, query):
        return None
        id = self.text2id(query)
        try:
            hit = self.es.get(index=self.INDEX, id=id, doc_type=type)
        except:
            hit = None
        if not hit: return None
        return json.loads(hit['_source']['result'])

    def set(self, type, query, qresult):
        id = self.text2id(query)
        self.es.index(index=self.INDEX,
                      doc_type=type,
                      id=id,
                      body={ "query" : query, "result": json.dumps(qresult)})

    def text2id(self, text):
        return uuid.uuid5(uuid.NAMESPACE_URL, text)


