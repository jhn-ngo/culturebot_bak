import argparse
from graph.kngraph import Graph
from config import Config
import logging

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--limit', '-l', type=int, default=0, help="IDs  limit")
    parser.add_argument('--skip', '-s', type=int, default=0, help="IDs  to skip")
    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)
    args = parse_args()
    graph = Graph(grakn_url=Config.GRAPH_URL, es_url=Config.SEARCH_URL)
    graph.clean_ids(limit=args.limit, skip=args.skip)
