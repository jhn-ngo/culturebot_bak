import datetime

class Profile():
    profiles = dict()
    def __init__(self,what):
        self.what = what
        self.started = datetime.datetime.now()
        self.delta = datetime.timedelta(0)

    def start(self):
        self.started = datetime.datetime.utcnow()

    def stop(self):
        self.delta += datetime.datetime.utcnow() - self.started

    def __str__(self):
        return self.what + ": " + str(self.delta)

    @staticmethod
    def getProfile(what):
        if not Profile.profiles.get(what):
            Profile.profiles[what] = Profile(what)
        return Profile.profiles[what]


    @staticmethod
    def printAll():
        for k,p in sorted(Profile.profiles.items()):
            print("Timer: {}".format(p))