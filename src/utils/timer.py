import logging
from timeit import default_timer as timer

class Timer:
  def __init__(self, name = "", log_level=logging.INFO):
    self.stages = dict()
    self.ts = dict()
    self.name = name
    self.log_level = log_level

  def start(self, stage):
    if not stage in self.stages:
      self.stages[stage] = 0
    self.ts[stage] = timer()

  def stop(self, stage):
    if not stage in self.stages or not stage in self.ts:
      return;
    self.stages[stage] += timer() - self.ts[stage]
    del self.ts[stage]

  def print(self):
    for stage,ts in self.stages.items():
      logging.log(self.log_level, "==== Execution time of stage {}::{}:{}".format(self.name,stage, ts))

  def __del__(self):
    self.print()
     
