from bonobo.config import Configurable, Service, Option
from graph.kngraph import Graph
import argparse

class GraphLoad(Configurable):
    batch_size = Option(required=False, default=100)
    graph_url = Option(required=False, default="localhost:48555")

    def __init__(self):
        super().__init__()
        self.graph = Graph(self.graph_url)
        self.count = 0

    def __call__(self, row):
        self.graph.insert(row)
        self.count += 1

        if self.count % self.batch_size == 0:
            self.commit()

    def commit(self):
       self.graph.commit()

    #def __del__(self):
    #    self.graph.commit()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i', type=str, default="europeana.json", help="Input JSON file")
    parser.add_argument('--skip', '-s', type=int, default=0, help="Number of rows to skip")
    parser.add_argument('--limit', '-l', type=int, default=0, help="Limit number of rows to load")
    return parser.parse_args()


if __name__ == "__main__":
    import bonobo, sys
    args = parse_args()

    def get_graph():
        graph = bonobo.Graph()
        graph.add_chain(bonobo.JsonReader(path=sys.argv[1]), GraphLoad())
        return graph

    # bonobo.run(get_graph())
    import json, logging
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.INFO)

    logging.info("Loading {}".format(sys.argv[1]))
    js = json.load(open(args.input, "r"))
    gl = GraphLoad()
    logging.info("Adding objects... {}".format(len(js)))
    i = 0
    for row in js:
        i += 1
        if i < args.skip: continue
        if args.limit and i > args.limit: break
        #try:
        gl(row)
        #except:
        #  logging.error("Exception while adding: {}".format(row))
        #  gl.graph._close_session()
        #if i % 100 == 0:

        logging.info("Added {} objects".format(i))
    gl.graph.commit() # commit of the last batch
