from bonobo.config import Configurable, Service, Option
import requests
import argparse
from pprint import pprint

class EuropeanaExtract(Configurable):
    query = Option(positional=True, default="Amsterdam")
    qf = Option(required=False, default="")
    theme = Option(required=False, default="")
    limit = Option(required=False, default=24)
    api_key = Option(required=False, default="UpLZkGEc2")
    eu_endpoint = Option(required=False, default="https://www.europeana.eu/api/v2")
    resource_prefix = 'https://www.europeana.eu/api/v2/record'
    use_cursor = Option(required=False, default=True)

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.cursor = "*" if self.use_cursor else None
        self.start = 0

    def __call__(self, **kwargs):
        for item in  self._search(self.query, cursor=self.cursor, **kwargs):
            yield item


    def _search(self, query, **kwargs):
        def gen_search_results():
            batch_size = 12

            while True:
                args = {
                    'wskey': self.api_key,
                    'query': query,
                    'qf': self.qf,
                    'rows': str(batch_size),
                    'profile': 'standard',
                    'theme': self.theme
                }
                for key, value in kwargs.items():
                    args[key] = value
                pprint(args)
                results = requests.get("{}/search.json".format(self.eu_endpoint), params=args).json()
                #pprint(results)
                if "nextCursor" in results:
                    self.cursor = results["nextCursor"]
                    kwargs["cursor"] = results["nextCursor"]
                else:
                    self.start +=  batch_size
                    kwargs["start"] = self.start
                for item in results["items"]:
                    yield item
                if self.start > self.limit: break

        item_count = 0
        for item in gen_search_results():
            item_count += 1
            print("ITEM COUNT: {}".format(item_count))
            if item_count > self.limit:
                break
            record = self._record(item["id"])
            record["search_result"] = item
            yield record


    def _record(self, record_id):
        args = {
            'wskey': self.api_key,
            'profile': 'full'
        }
        url = "%s%s.json" % ("{}/record".format(self.eu_endpoint), record_id)

        result = requests.get(url, params=args).json()
        return result

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--query', '-q', type=str, default="http://localhost", help="Search query (Europeana Search API format)")
    parser.add_argument('--limit', '-l', type=int, default=100, help="Search results limit")
    parser.add_argument('--output', '-o', type=str, default="europeana.json", help="Output JSON file")
    parser.add_argument('--theme', '-t', type=str, help="Theme: photography,nature,music")
    parser.add_argument('--query_filter', '-qf', type=str, help='Filter query (for ex.: what:"books"')
    parser.add_argument('--no_cursor', '-nc', action="store_true",  help='Do not use cursor (slow but sorted by relevancy"')
    return parser.parse_args()

if __name__ == "__main__":
    import bonobo, sys
    args = parse_args()
    print("AGS: {}".format(args))
    def get_graph():
        graph = bonobo.Graph()
        graph.add_chain(EuropeanaExtract(query=args.query, qf=args.query_filter, theme=args.theme, limit=args.limit, use_cursor= not args.no_cursor), bonobo.JsonWriter(path=args.output))
        return graph

    bonobo.run(get_graph())
