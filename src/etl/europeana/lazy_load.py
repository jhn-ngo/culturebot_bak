import bonobo
from bonobo.config import Configurable, Service, Option
from etl.europeana.extract import EuropeanaExtract
from etl.europeana.transform import EuropeanaTransform
from etl.load import GraphLoad

class EuropeanaLazyLoad:
    def __init__(self):
        pass

    def __call__(self, *args, **kwargs):
        graph,loader = self._get_graph(**kwargs)
        bonobo.run(graph)
        loader.commit()

    def _get_graph(self, **kwargs):
        graph = bonobo.Graph()
        gl = GraphLoad()
        # TODO: extract only qf="LANGUAGE:(en OR es OR it OR pl OR nl)"
        graph.add_chain(EuropeanaExtract(query=kwargs["query"], qf=kwargs.get("qf"), limit=kwargs["limit"], use_cursor=False),
                        EuropeanaTransform(), gl)
        return graph,gl


if __name__ == "__main__":
    import sys
    ell = EuropeanaLazyLoad()
    ell(query=sys.argv[1], limit=10)

