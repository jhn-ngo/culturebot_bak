# culturebot
== Set virtual environment
cd /opt/culturebot
source venv/bin/activate

== Build ==
cd chatbot/

* NLU (should be run after nlu.md is changed)
make train-nlu

* Core (should be run after stories.md or domain.yml  is changed)
make train-core

=== (Re)run ===
sudo supervisorctl restart all

Log file: /var/log/culturebot/culturebot.log

